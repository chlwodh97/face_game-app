import 'package:flutter/material.dart';

class PageFaceGame extends StatefulWidget {
  const PageFaceGame({super.key, required this.friendName});

  final String friendName;

  @override
  State<PageFaceGame> createState() => _PageFaceGameState();
}

@override
class _PageFaceGameState extends State<PageFaceGame> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('님 얼굴'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          child: Column(
        children: [
          Center(
            child: Stack(
              children: [
                Image.asset('assets/mouse_1.png'),
                Image.asset('assets/eyes_1.png'),
                Image.asset('assets/face_1.png'),
                Image.asset('assets/eyeborw_1.png'),
                Image.asset('assets/nose_2.png'),
              ],
            ),
          ),
          Container(
            alignment: Alignment(0, 0),
            margin: EdgeInsets.only(top: 200,),
            width: 120,
            height: 60,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.blueAccent),
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.blueGrey),
            child: Text(widget.friendName,
                style: TextStyle(fontSize: 20, color: Colors.white)),
          ),
        ],
      )),
    );
  }
}
