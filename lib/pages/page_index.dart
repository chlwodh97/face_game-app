import 'package:face_game_app/pages/page_face_game.dart';
import 'package:flutter/material.dart';




class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  TextEditingController tec = TextEditingController();



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('너 얼굴'),
      ),
      body: _buildBody(context),
    );
  }



  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(0, 50, 0, 0),

            child:
            TextField(
              controller: tec,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: '당신의 이름은?',
              ),
            ),
          ),

          Container(
            margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PageFaceGame(friendName: tec.text)));
              },
              child: Text('시작'),
            ),
          )
        ],
      ),


    );
  }
}


